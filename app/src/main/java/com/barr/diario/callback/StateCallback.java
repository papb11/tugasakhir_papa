package com.barr.diario.callback;

public interface StateCallback<T> {
    void onLoading();
    void onSuccess(T data);
    void onError(String message);
    void onEmpty();
}

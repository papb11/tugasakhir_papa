package com.barr.diario.features.auth.login;


import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.FragmentLoginBinding;
import com.barr.diario.features.auth.register.RegisterFragment;
import com.barr.diario.features.main.MainActivity;

import java.util.Objects;

import kotlin.Unit;

public class LoginFragment extends Fragment {

    private FragmentLoginBinding binding;

    private LoginViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        binding.btnLogin.setOnClickListener(v -> {
            String email = binding.edtEmail.getEditText().getText().toString();
            String password = binding.edtPassword.getEditText().getText().toString();

            viewModel.login(email, password, new StateCallback<Unit>() {
                @Override
                public void onLoading() {
                    binding.pbLogin.setVisibility(View.VISIBLE);
                }

                @Override
                public void onSuccess(Unit data) {
                    binding.pbLogin.setVisibility(View.GONE);
                    Intent intent = new Intent(requireActivity(), MainActivity.class);
                    startActivity(intent);
                    requireActivity().finish();
                }

                @Override
                public void onError(String message) {
                    binding.pbLogin.setVisibility(View.GONE);
                    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEmpty() {
                    binding.pbLogin.setVisibility(View.GONE);
                    Toast.makeText(requireContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            });

        });

        binding.tvHaveNoAccount.setOnClickListener(v -> {
            requireActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_auth, new RegisterFragment())
                    .commitNow();
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        });
    }
}
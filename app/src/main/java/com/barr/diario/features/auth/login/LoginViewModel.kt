package com.barr.diario.features.auth.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.barr.diario.callback.StateCallback
import com.barr.diario.data.FirebaseResponse
import com.barr.diario.data.FirebaseService
import com.barr.diario.data.RetrofitService
import com.barr.diario.model.BaseResponse
import com.barr.diario.model.user.UserResponse
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel(
    application: Application
): AndroidViewModel(application) {
    
    private val firebaseService = FirebaseService(application)
    private val apiService = RetrofitService.create()
    
    fun login(email: String, password: String, callback: StateCallback<Unit>) {
        callback.onLoading()
        viewModelScope.launch {
            firebaseService.signInWithEmailAndPassword(email, password).collect { firebaseResponse ->
                when(firebaseResponse) {
                    FirebaseResponse.Empty -> {
                        callback.onEmpty()
                    }
                    is FirebaseResponse.Error -> {
                        callback.onError(firebaseResponse.errorMessage)
                    }
                    is FirebaseResponse.Success -> {
                        apiService.getUser(firebaseResponse.data).enqueue(object : Callback<BaseResponse<UserResponse>> {
                            override fun onResponse(
                                call: Call<BaseResponse<UserResponse>>,
                                response: Response<BaseResponse<UserResponse>>
                            ) {
                                callback.onSuccess(Unit)
                            }
    
                            override fun onFailure(
                                call: Call<BaseResponse<UserResponse>>,
                                t: Throwable
                            ) {
                                callback.onError(t.message)
                            }
    
                        })
                    }
                }
            }
        }
    }
    
}
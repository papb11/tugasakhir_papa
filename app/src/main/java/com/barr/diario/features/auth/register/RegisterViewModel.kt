package com.barr.diario.features.auth.register

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.barr.diario.callback.StateCallback
import com.barr.diario.data.FirebaseResponse
import com.barr.diario.data.FirebaseService
import com.barr.diario.data.RetrofitService
import com.barr.diario.model.BaseResponse
import com.barr.diario.model.user.UserBody
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterViewModel(
    application: Application
): AndroidViewModel(application) {
    
    val firebaseService = FirebaseService(application)
    val apiService = RetrofitService.create()
    
    fun register(email: String, password: String, body: UserBody, callback: StateCallback<Unit>) {
        callback.onLoading()
        viewModelScope.launch {
            firebaseService.createUserWithEmailAndPassword(email, password).collect { firebaseResponse ->
                when(firebaseResponse) {
                    FirebaseResponse.Empty -> {
                        callback.onEmpty()
                    }
                    is FirebaseResponse.Error -> {
                        callback.onError(firebaseResponse.errorMessage)
                    }
                    is FirebaseResponse.Success -> {
                        body.uid = firebaseResponse.data
                        apiService.postNewUser(body).enqueue(object : Callback<BaseResponse<String>> {
                            override fun onResponse(
                                call: Call<BaseResponse<String>>,
                                response: Response<BaseResponse<String>>
                            ) {
                                callback.onSuccess(Unit)
                            }
    
                            override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                                callback.onError(t.message)
                            }
    
                        })
                    }
                }
            }
        }
    }
    
}
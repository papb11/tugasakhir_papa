package com.barr.diario.features.detail;

import androidx.lifecycle.ViewModel;

import com.barr.diario.callback.StateCallback;
import com.barr.diario.data.ApiService;
import com.barr.diario.data.RetrofitService;
import com.barr.diario.model.BaseResponse;
import com.barr.diario.model.diary.DiaryResponse;
import com.google.firebase.auth.FirebaseAuth;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailViewModel extends ViewModel {

    private ApiService apiService = RetrofitService.create();
    private String uid = FirebaseAuth.getInstance().getUid();

    public void getDiaryDetail(String diaryId, StateCallback<DiaryResponse> callback) {
        callback.onLoading();
        apiService.getDiaryDetail(diaryId).enqueue(new Callback<BaseResponse<DiaryResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<DiaryResponse>> call, Response<BaseResponse<DiaryResponse>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<DiaryResponse>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public void deleteDiary(String diaryId, StateCallback<String> callback) {
        apiService.deleteDiary(diaryId).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

}

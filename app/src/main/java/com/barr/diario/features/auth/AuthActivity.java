package com.barr.diario.features.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.barr.diario.R;
import com.barr.diario.databinding.ActivityAuthBinding;
import com.barr.diario.features.auth.login.LoginFragment;

public class AuthActivity extends AppCompatActivity {

    private ActivityAuthBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAuthBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportActionBar().hide();

        getSupportFragmentManager().beginTransaction()
                .add(binding.fragmentContainerAuth.getId(), new LoginFragment())
                .commit();
    }
}
package com.barr.diario.features.edit_diary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.ActivityEditDiaryBinding;
import com.barr.diario.model.diary.DiaryBody;
import com.barr.diario.util.Constant;

public class EditDiaryActivity extends AppCompatActivity {

    private ActivityEditDiaryBinding binding;
    private EditDiaryViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditDiaryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String dairyId = getIntent().getStringExtra(Constant.EXTRA_DIARY_ID);
        String title = getIntent().getStringExtra(Constant.VALUE_DIARY_TITLE);
        String content = getIntent().getStringExtra(Constant.VALUE_DIARY_CONTENT);
        binding.tietTitle.setText(title);
        binding.tietContent.setText(content);

        viewModel = new ViewModelProvider(this).get(EditDiaryViewModel.class);

        binding.btnSave.setOnClickListener(v -> {
            String newTitle = binding.tietTitle.getText().toString();
            String newContent = binding.tietContent.getText().toString();
            DiaryBody body = new DiaryBody();
            body.setTitle(newTitle);
            body.setContent(newContent);
            viewModel.updateDiary(dairyId, newTitle, newContent, new StateCallback<String>() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onSuccess(String data) {
                    finish();
                }

                @Override
                public void onError(String message) {
                    Toast.makeText(EditDiaryActivity.this, message, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEmpty() {

                }
            });
        });
    }
}
package com.barr.diario.features.profile

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.barr.diario.callback.StateCallback
import com.barr.diario.data.FirebaseResponse
import com.barr.diario.data.FirebaseService
import com.barr.diario.data.RetrofitService
import com.barr.diario.model.BaseResponse
import com.barr.diario.model.user.UpdateUserPhotoProfile
import com.barr.diario.model.user.UserResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileViewModel(
    application: Application
): AndroidViewModel(application) {
    
    private val firebaseService = FirebaseService(application)
    private val apiService = RetrofitService.create()
    private val firebaseAuth = FirebaseAuth.getInstance()
    private val uid = FirebaseAuth.getInstance().uid
    
    fun getUser(callback: StateCallback<UserResponse>) {
        apiService.getUser(uid!!).enqueue(object : Callback<BaseResponse<UserResponse>> {
            override fun onResponse(
                call: Call<BaseResponse<UserResponse>>,
                response: Response<BaseResponse<UserResponse>>
            ) {
                if (response.isSuccessful) {
                    val userResponse = response.body()?.data
                    if (userResponse != null) {
                        callback.onSuccess(userResponse)
                    } else {
                        callback.onEmpty()
                    }
                } else {
                    callback.onError(response.message())
                }
            }
    
            override fun onFailure(call: Call<BaseResponse<UserResponse>>, t: Throwable) {
                callback.onError(t.message)
            }
    
        })
    }
    
    fun updatePhotoProfile(photoLocation: Uri, callback: StateCallback<String>) {
        viewModelScope.launch {
            firebaseService.uploadAvatar(photoLocation).collect { firebaseResponse ->
                when(firebaseResponse) {
                    FirebaseResponse.Empty -> { callback.onEmpty() }
                    is FirebaseResponse.Error -> { callback.onError(firebaseResponse.errorMessage) }
                    is FirebaseResponse.Success -> {
                        val body = UpdateUserPhotoProfile()
                        body.avatarUrl = firebaseResponse.data
                        apiService.putPhotoProfile(uid!!, body).enqueue(object : Callback<BaseResponse<String>> {
                            override fun onResponse(
                                call: Call<BaseResponse<String>>,
                                response: Response<BaseResponse<String>>
                            ) {
                                if (response.isSuccessful) {
                                    callback.onSuccess(firebaseResponse.data)
                                } else {
                                    callback.onError(response.message())
                                }
                            }
    
                            override fun onFailure(call: Call<BaseResponse<String>>, t: Throwable) {
                                callback.onError(t.message)
                            }
                        })
                    }
                }
            }
        }
    }
    
    fun logout() {
        firebaseAuth.signOut()
    }
    
}
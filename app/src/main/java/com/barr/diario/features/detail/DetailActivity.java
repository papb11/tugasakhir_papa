package com.barr.diario.features.detail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.ActivityDetailBinding;
import com.barr.diario.features.edit_diary.EditDiaryActivity;
import com.barr.diario.model.diary.DiaryResponse;
import com.barr.diario.util.Constant;

public class DetailActivity extends AppCompatActivity {

    private ActivityDetailBinding binding;
    private DetailViewModel viewModel;
    private String diaryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        diaryId = getIntent().getStringExtra(Constant.EXTRA_DIARY_ID);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        viewModel.getDiaryDetail(diaryId, diaryCallback);

        binding.btnDelete.setOnClickListener(v -> {
            viewModel.deleteDiary(diaryId, deleteCallback);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            //do intent
            Intent intent = new Intent(this, EditDiaryActivity.class);
            intent.putExtra(Constant.EXTRA_DIARY_ID, diaryId);
            intent.putExtra(Constant.VALUE_DIARY_TITLE, binding.tvTitle.getText().toString());
            intent.putExtra(Constant.VALUE_DIARY_CONTENT, binding.tvContent.getText().toString());
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private StateCallback<DiaryResponse> diaryCallback = new StateCallback<DiaryResponse>() {
        @Override
        public void onLoading() {
            binding.pbDetail.setVisibility(View.VISIBLE);
            binding.btnDelete.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onSuccess(DiaryResponse result) {
            binding.pbDetail.setVisibility(View.INVISIBLE);
            binding.btnDelete.setVisibility(View.VISIBLE);
            binding.tvTitle.setText(result.getTitle());
            binding.tvAuthor.setText("By: " + result.getName());
            binding.tvContent.setText(result.getContent());
        }

        @Override
        public void onError(String message) {
            binding.pbDetail.setVisibility(View.INVISIBLE);
            Toast.makeText(DetailActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEmpty() {

        }
    };

    private StateCallback<String> deleteCallback = new StateCallback<String>() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onSuccess(String data) {
            finish();
        }

        @Override
        public void onError(String message) {
            Toast.makeText(DetailActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEmpty() {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getDiaryDetail(getIntent().getStringExtra(Constant.EXTRA_DIARY_ID), diaryCallback);
    }
}
package com.barr.diario.features.profile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.ActivityProfileBinding;
import com.barr.diario.features.auth.AuthActivity;
import com.barr.diario.features.main.MainActivity;
import com.barr.diario.model.user.UserResponse;
import com.bumptech.glide.Glide;

public class ProfileActivity extends AppCompatActivity {

    private ActivityProfileBinding binding;
    private ProfileViewModel viewModel;

    private Integer photoMax = 1;
    private Uri photoLocation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(ProfileViewModel.class);

        viewModel.getUser(userCallback);

        binding.btnChangePhoto.setOnClickListener(view -> {
            findPhoto();
        });

        binding.btnLogout.setOnClickListener(view -> {
            viewModel.logout();
            Intent intent = new Intent(this, AuthActivity.class);
            startActivity(intent);
            finish();
        });
    }

    private void findPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == photoMax && resultCode == RESULT_OK && data != null && data.getData() != null) {
            photoLocation = data.getData();
            viewModel.updatePhotoProfile(photoLocation, photoCallback);
        }
    }

    private StateCallback<UserResponse> userCallback = new StateCallback<UserResponse>() {

        @Override
        public void onLoading() {

        }

        @Override
        public void onSuccess(UserResponse data) {
            Glide.with(ProfileActivity.this)
                    .load(data.getAvatarUrl())
                    .circleCrop()
                    .placeholder(R.drawable.ic_default_ava)
                    .into(binding.ivAvatar);
            binding.tvName.setText(data.getName());
            binding.tvEmail.setText(data.getEmail());
        }

        @Override
        public void onError(String message) {
            Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEmpty() {
            Toast.makeText(ProfileActivity.this, "Something Went Wrong!", Toast.LENGTH_SHORT).show();
        }
    };

    private StateCallback<String> photoCallback = new StateCallback<String>() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onSuccess(String data) {
            Glide.with(ProfileActivity.this)
                    .load(data)
                    .circleCrop()
                    .placeholder(R.drawable.ic_default_ava)
                    .into(binding.ivAvatar);
        }

        @Override
        public void onError(String message) {
            Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEmpty() {
            Toast.makeText(ProfileActivity.this, "Something Went Wrong!", Toast.LENGTH_SHORT).show();
        }
    };
}
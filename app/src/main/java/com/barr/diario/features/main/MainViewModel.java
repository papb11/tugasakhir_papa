package com.barr.diario.features.main;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.barr.diario.callback.StateCallback;
import com.barr.diario.data.ApiService;
import com.barr.diario.data.RetrofitService;
import com.barr.diario.model.BaseResponse;
import com.barr.diario.model.diary.DiaryResponse;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {

    private ApiService apiService = RetrofitService.create();
    private String uid = FirebaseAuth.getInstance().getUid();

    public void getDiaries(StateCallback<List<DiaryResponse>> callback) {
        callback.onLoading();
        apiService.getUserDiaries(uid).enqueue(new Callback<BaseResponse<List<DiaryResponse>>>() {
            @Override
            public void onResponse(
                    @NonNull Call<BaseResponse<List<DiaryResponse>>> call,
                    @NonNull Response<BaseResponse<List<DiaryResponse>>> response
            ) {
                if(response.isSuccessful()) {
                    if(response.body() != null) {
                        List<DiaryResponse> result = response.body().getData();
                        if(result.isEmpty()) {
                            callback.onEmpty();
                        } else {
                            callback.onSuccess(result);
                        }
                    }
                } else {
                    callback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<DiaryResponse>>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

}

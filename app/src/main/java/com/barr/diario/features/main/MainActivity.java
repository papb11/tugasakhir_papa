package com.barr.diario.features.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.adapter.DiaryAdapter;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.ActivityMainBinding;
import com.barr.diario.features.add_diary.AddDiaryActivity;
import com.barr.diario.features.profile.ProfileActivity;
import com.barr.diario.model.diary.DiaryResponse;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private MainViewModel viewModel;
    private DiaryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        adapter = new DiaryAdapter();

        binding.rvDiaries.setAdapter(adapter);
        binding.rvDiaries.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        viewModel.getDiaries(diariesCallback);

        binding.fabAdd.setOnClickListener(v -> {
            Intent intent = new Intent(this, AddDiaryActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_profile) {
            //do intent
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private StateCallback<List<DiaryResponse>> diariesCallback = new StateCallback<List<DiaryResponse>>() {
        @Override
        public void onLoading() {
            binding.pbMain.setVisibility(View.VISIBLE);
            binding.rvDiaries.setVisibility(View.GONE);
            binding.tvEmptyState.setVisibility(View.GONE);
        }

        @Override
        public void onSuccess(List<DiaryResponse> result) {
            binding.pbMain.setVisibility(View.GONE);
            binding.rvDiaries.setVisibility(View.VISIBLE);
            binding.tvEmptyState.setVisibility(View.GONE);
            adapter.submitList(result);
        }

        @Override
        public void onError(String message) {
            binding.pbMain.setVisibility(View.GONE);
            binding.rvDiaries.setVisibility(View.GONE);
            binding.tvEmptyState.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEmpty() {
            binding.pbMain.setVisibility(View.GONE);
            binding.rvDiaries.setVisibility(View.GONE);
            binding.tvEmptyState.setVisibility(View.VISIBLE);
            adapter.submitList(new ArrayList<>());
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getDiaries(diariesCallback);
    }
}
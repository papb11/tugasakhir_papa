package com.barr.diario.features.auth.register;

import androidx.activity.OnBackPressedCallback;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.FragmentRegisterBinding;
import com.barr.diario.features.auth.login.LoginFragment;
import com.barr.diario.features.main.MainActivity;
import com.barr.diario.model.user.UserBody;

import kotlin.Unit;

public class RegisterFragment extends Fragment {

    private FragmentRegisterBinding binding;

    private RegisterViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentRegisterBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);

        binding.btnRegister.setOnClickListener(v -> {
            String email = binding.edtEmail.getEditText().getText().toString();
            String name = binding.edtName.getEditText().getText().toString();
            String password = binding.edtPassword.getEditText().getText().toString();

            UserBody body = new UserBody();
            body.setEmail(email);
            body.setName(name);

            viewModel.register(email, password, body, new StateCallback<Unit>() {
                @Override
                public void onLoading() {
                    binding.pbRegister.setVisibility(View.VISIBLE);
                }

                @Override
                public void onSuccess(Unit data) {
                    binding.pbRegister.setVisibility(View.GONE);
                    Intent intent = new Intent(requireActivity(), MainActivity.class);
                    startActivity(intent);
                    requireActivity().finish();
                }

                @Override
                public void onError(String message) {
                    binding.pbRegister.setVisibility(View.GONE);
                    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEmpty() {
                    binding.pbRegister.setVisibility(View.GONE);
                    Toast.makeText(requireContext(), "Something Went Wrong!", Toast.LENGTH_SHORT).show();
                }
            });
        });

        binding.tvHaveAccount.setOnClickListener(v -> {
            requireActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_auth, new LoginFragment())
                    .commitNow();
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container_auth, new LoginFragment())
                        .commitNow();
            }
        });
    }
}
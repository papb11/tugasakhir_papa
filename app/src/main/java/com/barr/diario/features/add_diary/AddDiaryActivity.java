package com.barr.diario.features.add_diary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Toast;

import com.barr.diario.R;
import com.barr.diario.callback.StateCallback;
import com.barr.diario.databinding.ActivityAddDiaryBinding;

public class AddDiaryActivity extends AppCompatActivity {

    private ActivityAddDiaryBinding binding;
    private AddDiaryViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddDiaryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(AddDiaryViewModel.class);

        binding.btnSave.setOnClickListener(v -> {
            String title = binding.tietTitle.getText().toString();
            String content = binding.tietContent.getText().toString();

            viewModel.addDiary(title, content, new StateCallback<String>() {
                @Override
                public void onLoading() {

                }

                @Override
                public void onSuccess(String data) {
                    finish();
                }

                @Override
                public void onError(String message) {
                    Toast.makeText(AddDiaryActivity.this, message, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onEmpty() {

                }
            });
        });

    }
}
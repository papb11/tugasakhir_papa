package com.barr.diario.features.add_diary;

import androidx.lifecycle.ViewModel;

import com.barr.diario.callback.StateCallback;
import com.barr.diario.data.ApiService;
import com.barr.diario.data.RetrofitService;
import com.barr.diario.model.BaseResponse;
import com.barr.diario.model.diary.DiaryBody;
import com.google.firebase.auth.FirebaseAuth;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDiaryViewModel extends ViewModel {

    private ApiService apiService = RetrofitService.create();
    private String uid = FirebaseAuth.getInstance().getUid();

    public void addDiary(String title, String content, StateCallback<String> callback) {
        DiaryBody body = new DiaryBody();
        body.setUid(uid);
        body.setTitle(title);
        body.setContent(content);

        apiService.postNewDiary(body).enqueue(new Callback<BaseResponse<String>>() {

            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

}

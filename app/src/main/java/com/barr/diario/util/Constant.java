package com.barr.diario.util;

public class Constant {

    public static final String EXTRA_DIARY_ID = "extra_diary_id";
    public static final String VALUE_DIARY_TITLE = "value_diary_title";
    public static final String VALUE_DIARY_CONTENT = "value_diary_content";

}

package com.barr.diario.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.barr.diario.databinding.ItemListDiaryBinding;
import com.barr.diario.features.detail.DetailActivity;
import com.barr.diario.model.diary.DiaryResponse;
import com.barr.diario.util.Constant;

import java.util.ArrayList;
import java.util.List;

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.DiaryViewHolder> {

    private List<DiaryResponse> diaries = new ArrayList<>();

    public void submitList(List<DiaryResponse> data) {
        diaries.clear();
        diaries.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DiaryAdapter.DiaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemListDiaryBinding view = ItemListDiaryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new DiaryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DiaryAdapter.DiaryViewHolder holder, int position) {
        holder.bind(diaries.get(position));
    }

    @Override
    public int getItemCount() {
        return diaries.size();
    }

    public class DiaryViewHolder extends RecyclerView.ViewHolder {

        private ItemListDiaryBinding binding;

        public DiaryViewHolder(@NonNull ItemListDiaryBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        void bind(DiaryResponse diary) {
            binding.tvTitle.setText(diary.getTitle());
            binding.tvContent.setText(diary.getContent());

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                intent.putExtra(Constant.EXTRA_DIARY_ID, diary.getDiaryId());
                itemView.getContext().startActivity(intent);
            });
        }

    }
}

package com.barr.diario.data

import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.tasks.await

class FirebaseService(
    private val context: Context
) {
    
    val auth = Firebase.auth
    val storage = Firebase.storage
    fun getCurrentUserId() = auth.currentUser?.uid
    
    fun createUserWithEmailAndPassword(
        email: String,
        password: String
    ): Flow<FirebaseResponse<String>> =
        flow {
            val createUser = auth.createUserWithEmailAndPassword(email, password).await()
            val user = createUser.user
            if (user != null) {
                emit(FirebaseResponse.Success(user.uid))
            } else {
                emit(FirebaseResponse.Empty)
            }
        }.catch {
            emit(FirebaseResponse.Error(it.message.toString()))
        }.flowOn(Dispatchers.IO)
    
    
    fun signInWithEmailAndPassword(
        email: String,
        password: String
    ): Flow<FirebaseResponse<String>> =
        flow {
            val singInUser = auth.signInWithEmailAndPassword(email, password).await()
            val user = singInUser.user
            if (user != null) {
                emit(FirebaseResponse.Success(user.uid))
            } else {
                emit(FirebaseResponse.Empty)
            }
        }.catch {
            emit(FirebaseResponse.Error(it.message.toString()))
        }.flowOn(Dispatchers.IO)
    
    fun uploadAvatar(
        photoLocation: Uri
    ) = flow {
        val contentResolver = context.contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        val fileExtension =
            mimeTypeMap.getExtensionFromMimeType(contentResolver?.getType(photoLocation))
        
        val storageRef = storage.reference.child("avatar_path")
        val storage = storageRef.child(
            System.currentTimeMillis()
                .toString() + "." + fileExtension
        )
        
        storage.putFile(photoLocation).await()
        val avatarUrl = storage.downloadUrl.await()
        
        if(avatarUrl != null) {
            emit(FirebaseResponse.Success(avatarUrl.toString()))
        } else {
            emit(FirebaseResponse.Empty)
        }
    }.catch {
        emit(FirebaseResponse.Error(it.message.toString()))
    }.flowOn(Dispatchers.IO)
}
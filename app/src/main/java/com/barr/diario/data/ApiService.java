package com.barr.diario.data;

import com.barr.diario.model.BaseResponse;
import com.barr.diario.model.diary.DiaryBody;
import com.barr.diario.model.diary.DiaryResponse;
import com.barr.diario.model.user.UpdateUserPhotoProfile;
import com.barr.diario.model.user.UserBody;
import com.barr.diario.model.user.UserResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiService {

    @POST("/user")
    public Call<BaseResponse<String>> postNewUser(@Body UserBody body);

    @GET("/user/{uid}")
    public Call<BaseResponse<UserResponse>> getUser(@Path("uid") String uid);

    @PUT("/user/{uid}/photo-profile")
    public Call<BaseResponse<String>> putPhotoProfile(@Path("uid") String uid, @Body UpdateUserPhotoProfile photoProfile);

    @POST("/diary")
    public Call<BaseResponse<String>> postNewDiary(@Body DiaryBody body);

    @GET("/user/{uid}/diary")
    public Call<BaseResponse<List<DiaryResponse>>> getUserDiaries(@Path("uid") String uid);

    @GET("/diary/{diaryId}")
    public Call<BaseResponse<DiaryResponse>> getDiaryDetail(@Path("diaryId") String diaryId);

    @PUT("/diary/{diaryId}")
    public Call<BaseResponse<String>> updateDiary(@Path("diaryId") String diaryId, @Body DiaryBody body);

    @DELETE("/diary/{diaryId}")
    public Call<BaseResponse<String>> deleteDiary(@Path("diaryId") String diaryId);
}

package com.barr.diario.model.user;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class UserResponse {
    @SerializedName("uid")
    String uid;
    @SerializedName("name")
    String name;
    @SerializedName("email")
    String email;
    @SerializedName("avatar_url")
    String avatarUrl;

    @NotNull
    public final String getUid() {
        return this.uid;
    }

    @NotNull
    public final String getName() {
        return this.name;
    }

    @NotNull
    public final String getEmail() {
        return this.email;
    }

    @NotNull
    public final String getAvatarUrl() {
        return this.avatarUrl;
    }
}

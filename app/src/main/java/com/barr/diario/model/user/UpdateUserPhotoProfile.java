package com.barr.diario.model.user;

import com.google.gson.annotations.SerializedName;

public class UpdateUserPhotoProfile {
    @SerializedName("avatar_url")
    String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}

package com.barr.diario.model.diary;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class DiaryResponse {

    @SerializedName("diary_id")
    private String diaryId;
    @SerializedName("name")
    private String name;
    @SerializedName("title")
    private String title;
    @SerializedName("content")
    private String content;

    @NotNull
    public final String getDiaryId() {
        return this.diaryId;
    }

    @NotNull
    public final String getName() {
        return this.name;
    }

    @NotNull
    public final String getTitle() {
        return this.title;
    }

    @NotNull
    public final String getContent() {
        return this.content;
    }
}

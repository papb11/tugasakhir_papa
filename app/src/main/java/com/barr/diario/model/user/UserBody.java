package com.barr.diario.model.user;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class UserBody {
    @SerializedName("uid")
    String uid;
    @SerializedName("name")
    String name;
    @SerializedName("email")
    String email;

    @NotNull
    public final String getUid() {
        return this.uid;
    }

    @NotNull
    public final String getName() {
        return this.name;
    }

    @NotNull
    public final String getEmail() {
        return this.email;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

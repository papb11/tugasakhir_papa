package com.barr.diario.model.diary;

import com.google.gson.annotations.SerializedName;

public class DiaryBody {

    @SerializedName("uid")
    String uid;

    @SerializedName("title")
    String title;

    @SerializedName("content")
    String content;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
